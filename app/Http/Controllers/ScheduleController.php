<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;
use Carbon\Carbon;
use Validator;
class ScheduleController extends Controller
{
    private $days,$convert_days;

    public function __construct()
    {
        $this->days = [
            6 => 'SATURDAY',
            0 => 'SUNDAY',
            1 => 'MONDAY',
            2 => 'TUESDAY',
            3 => 'WEDNESDAY',
            4 => 'THURSDAY',
            5 => 'FRIDAY',
        ];

        $this->convert_days = [
            1 => 6,
            2 => 0,
            3 => 1,
            4 => 2 ,
            5 => 3,
            6 => 4,
            7 => 5,
        ];


    }

    /*
     * Mohamed Mamdouh
     * Create Schedule Process
     * @params($start_date , int days[] , number_of_sessions_for_chapter )
     * return schedule
     */
    public function create_schedule(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'number_of_sessions' => 'required|Numeric|max:30',
            'number_of_days_per_week' => 'required|array'
        ]);

        if ($validator->fails()) {
            $this->response['process'] = false;
            $this->response['message'] = $validator->errors();
            return $this->response;
        }


        $start_date                     = $request->start_date;
        $days                           = $request->number_of_days_per_week;
        $number_of_sessions_for_chapter = $request->number_of_sessions;

        $total_number_of_sessions = $this->calculate_total_number_of_sessions(30, $number_of_sessions_for_chapter);
        $total_number_of_weeks = $this->calculate_total_number_of_weeks($total_number_of_sessions, count($days));

        $result = array();
        $session_number = 1;

        $nearest_date = $this->get_early_date($days,$start_date);
        $days = $nearest_date['new_array'];


        $result[0]['name'] = $this->get_the_day_by_date($start_date);
        $result[0]['date'] = $start_date;
        $result[0]['session_number'] = 1;
        $result[0]['remaining'] = $total_number_of_sessions-1;
        $temp_date = $result[0]['date'];

        $array_index = 1;
        $temp_date = $start_date;
        $counter =1;


        for($week = 1; $week <= $total_number_of_weeks+1; $week++){
            foreach ($days as $key => $day){
                if(($total_number_of_sessions - $session_number ) == 0)
                         break;

                Carbon::setTestNow($temp_date);

                $result[$array_index]['name'] = $this->get_day_by_number($day);
                $result[$array_index]['date'] = Carbon::parse('next '. $result[$key]['name'])->toDateString();
                $result[$array_index]['session_number'] = $session_number +1;
                $result[$array_index]['remaining'] =  $total_number_of_sessions - $session_number - 1;

                $temp_date = $result[$array_index]['date'];
                $session_number +=1;
                $array_index +=1;

                $counter += 1;
                Carbon::setTestNow();

            }

            if(($total_number_of_sessions - $session_number ) == 0)
                break;
        }




        return response(['schedule' => $result],200);

    }

    /*
     * Mohamed Mamddouh
     * Calculate Total number of Sessions
     * @params( number_of_chapters , number_of_sessions_for_one_chapter)
     * return total_number_of_sessions
     */

    public function calculate_total_number_of_sessions( $number_of_chapters , $number_of_sessions_for_one_chapter){

        return $number_of_chapters * $number_of_sessions_for_one_chapter;
    }

    /*
     * Mohamed Mamddouh
     * Calculate Total number of weeks
     * @params( number_of_chapters , number_of_sessions_for_one_chapter)
     * return total_number_of_weeks
     */

    public function calculate_total_number_of_weeks( $number_of_sessions , $number_of_days_per_week){

        return $number_of_sessions / $number_of_days_per_week;
    }

    /*
     * Mohamed Mamdouh
     * Get Day Name By Date
     *
     */

    public function get_the_day_by_date($day){
        $day = Carbon::parse($day);
        return $this->days[$day->dayOfWeek];
    }


    public function get_day_by_number($number){
        return $this->days[$this->convert_days[$number]];
    }

    /*
     * Mohamed Mamdouh
     * Get Nearest Day
     */



    public function get_early_date($days,$start_date){
        Carbon::setTestNow($start_date);
        $temp_start_date = Carbon::now();
        $diffDays = 7;
        $date ='';
        $index=0;
        $return_day= $days[0];

        foreach ($days as $key => $day){
            $name =  $this->get_day_by_number($day);
            if($temp_start_date->diffInDays(Carbon::parse('next '. $name) ) < $diffDays ){

                $diffDays = $temp_start_date->diffInDays(Carbon::parse('next '. $name));
                $date = Carbon::parse('next '. $name)->toDateString();

                $return_day = $day;
                $index = $key;
            }
        }



        return ['date' => $date,'day' => $return_day , 'start_index' => $index ,'new_array' => $this->resort_array_by_index($days, $index)];
    }


    /*
     * Sort The Array To start with the right day
     */


    public function resort_array_by_index($days , $index){
        $temp_date = [];
        for($i = $index; $i<count($days);$i++){
            $temp_date[] = $days[$i];
        }

        for($j = 0; $j <= $index-1; $j++){
            $temp_date[] = $days[$j];
        }

        return $temp_date;
    }

}
