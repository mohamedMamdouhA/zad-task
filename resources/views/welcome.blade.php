<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Zad Task</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
        <style>
            /* Customize the label (the container) */
            .label-container {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .label-container  input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #eee;
            }

            /* On mouse-over, add a grey background color */
            .label-container :hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .label-container  input:checked ~ .checkmark {
                background-color: #2196F3;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .label-container  input:checked ~ .checkmark:after {
                display: inline-block;
            }

            /* Style the checkmark/indicator */
            .label-container  .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
        </style>
    </head>
    <body>
        <div class="container" id="app">
            <h1 class="text-center">Zad Task</h1>
            <p>Task Description: A student required to finish a book of thirty chapters, he is allowed to choose when he starts, days he will be attending every week and a starting date.
                <br> <span style="color:red;"> Note that The Starting Date is included in the Schedule.</span>
                <br> <span style="color:red;"> Max Number of Session is 30.</span>

            </p>

            <form id="formData">
              <div class="row">

                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="">Starting Date</label>
                          <input class="datepicker form-control" type="text" name="start_date"  data-date-format='yyyy-mm-dd'>
                      </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="">How Many Sessions To complete one chapter</label>
                          <input class="form-control" name="number_of_sessions" type="number">
                      </div>
                  </div>
              </div>
                  <hr>
                <div class="row">
                  <div class="form-group">
                      <div class="col-md-3">
                          <label class="label-container ">SATURDAY
                              <input type="checkbox" value="1"  name="number_of_days_per_week[]" checked="checked">
                              <span class="checkmark"></span>
                          </label>
                      </div>


                      <div class="col-md-3">
                          <label class="label-container ">SUNDAY
                              <input type="checkbox" value="2" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                      <div class="col-md-3">
                          <label class="label-container ">MONDAY
                              <input type="checkbox" value="3" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                      <div class="col-md-3">
                          <label class="label-container ">TUESDAY
                              <input type="checkbox" value="4" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                      <div class="col-md-3">
                          <label class="label-container ">WEDNESDAY
                              <input type="checkbox" value="5" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                      <div class="col-md-3">
                          <label class="label-container ">THURSDAY
                              <input type="checkbox" value="6" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                      <div class="col-md-3">
                          <label class="label-container ">FRIDAY
                              <input type="checkbox" value="7" name="number_of_days_per_week[]">
                              <span class="checkmark"></span>
                          </label>
                      </div>
                  </div>

              </div>
                <hr>
                <input type="submit" class="btn btn-primary" @click="submitData" value="Create Schedule">

            </form>
            <hr>
            <div class="col-md-12">
                <table  class="table">
                    <thead>
                        <tr>
                            <th>Day number</th>
                            <th>Day name</th>
                            <th>Date</th>
                            <th>Session number</th>
                            <th>Remaining Sessions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(day , $index ) in table">
                            <td>@{{ $index +1 }}</td>
                            <td>@{{ day.name }}</td>
                            <td>@{{ day.date }}</td>
                            <td>@{{ day.session_number }}</td>
                            <td>@{{ day.remaining }}</td>

                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>

        <script>
            $(function(){
                $('.datepicker').datepicker();

            });
        </script>

        <script>
            var app = new Vue({
                el:'#app',
                data:{
                    table:[]
                },
                methods:{
                    submitData:function(e){
                        this.table = [];
                        e.preventDefault();
                        var data = $("#formData").serialize();
                        axios.post('/prepareSchedule',data).then(function(response){
                            if(response.data.process == false){
                                alert("Please Fill All Details");
                            }
                            else{
                                this.table = response.data.schedule;
                            }
                        }.bind(this)).catch(function(e){
                            console.log(e);
                        });
                    }
                }
            })
        </script>
    </body>
</html>
